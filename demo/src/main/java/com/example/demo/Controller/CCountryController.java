package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CCountry;
import com.example.demo.Respository.iCountryRespository;

@RestController
@CrossOrigin
@RequestMapping("country")
public class CCountryController {
    
    @Autowired 
    iCountryRespository iCountryRespository ;
    
    // lấy all country
    @GetMapping("/all")
        public ResponseEntity <List<CCountry>> getAllCountry() {
           
            try {
                List<CCountry> listCountry = new ArrayList<>();
                iCountryRespository.findAll().forEach(listCountry::add);

                if(listCountry.size() == 0){
                    return new ResponseEntity<List <CCountry>>(listCountry, HttpStatus.NOT_FOUND);
                }
                else{
                    return new ResponseEntity<List <CCountry>>(listCountry, HttpStatus.OK);
                }
            } catch (Exception e) {
               
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
       }

// tạo country
	@PostMapping("/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		
		try {
			CCountry newRole = new CCountry();
			
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = iCountryRespository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
		}
	}

// update country bởi id 
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = iCountryRespository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			CCountry savedCountry = iCountryRespository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

// delete country bởi id
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			Optional<CCountry> optional= iCountryRespository.findById(id);
			if (optional.isPresent()) {
				iCountryRespository.deleteById(id);
			}else {
				//countryRepository.deleteById(id);
			}			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

// tìm country bởi id
	@GetMapping("/details/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (iCountryRespository.findById(id).isPresent())
			return iCountryRespository.findById(id).get();
		else
			return null;
	}

}
