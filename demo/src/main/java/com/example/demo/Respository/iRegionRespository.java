package com.example.demo.Respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CRegion;



public interface iRegionRespository extends JpaRepository <CRegion , Long> {
    List <CRegion> findByCountryId(Long id);

    Optional<CRegion> findByIdAndCountryId (Long countryId , Long regionId);
}
