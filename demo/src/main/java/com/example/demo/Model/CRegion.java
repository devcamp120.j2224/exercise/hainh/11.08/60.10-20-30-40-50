package com.example.demo.Model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.*;



import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "region")
public class CRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "region_code")
    private String regionCode;
    
    @Column(name = "region_name")
    private String regionName;

    @ManyToOne
    @JsonBackReference
    private CCountry country;

    @OneToMany(mappedBy = "region", cascade = CascadeType.ALL)
    private Set<CDistrict> district;
    
   

    public CRegion() {
    }

    public CRegion(Long id, String regionCode, String regionName, CCountry country, Set<CDistrict> district) {
        this.id = id;
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.country = country;
        this.district = district;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getRegionName() {
        return regionName;
    }
    
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
 
    public CCountry getCountry() {
        return country;
    }

    public void setCountry(CCountry cCountry) {
        this.country = cCountry;
    }

    public Set<CDistrict> getDistrict() {
        return district;
    }

    public void setDistrict(Set<CDistrict> district) {
        this.district = district;
    }
}

